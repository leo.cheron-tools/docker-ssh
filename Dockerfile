FROM alpine:3.9

MAINTAINER Leo Cheron <leo@cheron.works>

# git
RUN apk --no-cache add bash git openssh rsync && \
    mkdir -p ~root/.ssh && chmod 700 ~root/.ssh/ && \
    echo -e "Port 22\n" >> /etc/ssh/sshd_config

# build dependencies
RUN apk --no-cache add python build-base

# git ftp
RUN apk --no-cache add curl && \
	git clone https://github.com/git-ftp/git-ftp.git && \
	cd git-ftp && \
	tag="$(git tag | grep '^[0-9]*\.[0-9]*\.[0-9]*$' | tail -1)" && \
	git checkout "$tag" && \
	make install && \
	cd .. && rm -r git-ftp

# cleanup
RUN apk del python build-base

EXPOSE 22
